<?php

namespace App\Http\Controllers;

use App\Http\Requests\DespesaUpdateRequest;
use App\Http\Requests\DespesaStoreRequest;
use App\Models\Despesa;
use App\Models\User;
use App\Notifications\DespesaUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;


class DespesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Despesa::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DespesaStoreRequest $request)
    {
        try {
            $user = User::select('id')->where('email', $request->email)->first();
            $requestAll = $request->all();
            $requestAll['user_id'] = $user->id;

            Notification::send($user, new DespesaUser($user));

            return response()->json([
                'status' => true,
                'message' => "Despesa created successfully",
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return response()->json([
                'status' => true,
                'despesa' => Despesa::findOrFail($id),
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DespesaUpdateRequest $request, $id)
    {
        try {
            $despesa = Despesa::findOrFail($id);
            $requestAll = $request->all();
            $requestAll['user_id'] = User::select('id')->where('email', $request->email)->first()->id;
            $despesa->update($requestAll);

            return response()->json([
                'status' => true,
                'message' => "Successfully updated",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            return Despesa::destroy($id);
            return response()->json([
                'status' => true,
                'message' => "Successfully deleted"
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
        
    }
}
