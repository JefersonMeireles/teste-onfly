<?php

namespace App\Http\Requests;

use Illuminate\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class DespesaUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $despesa = $this->route('despesa');
        return $this->user()->can('update', $despesa);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'descricao' => 'required|max:191',
            'data' => 'required|date|before:'.\Carbon\Carbon::now(),
            'valor' => 'required|numeric|min:0',
            'email' => 'required|exists:App\Models\User,email'
        ];
    }
}
