<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DespesaStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'descricao' => 'required|max:191',
            'data' => 'required|date|before:'.\Carbon\Carbon::now(),
            'valor' => 'required|numeric|min:0',
            'email' => 'required|exists:App\Models\User,email'
        ];
    }
}
